# A simple app that searches for Github and/or Gitlab profiles

Link: https://github-gitlab-profiles.netlify.app/

JavaScript, GitLab API, GitHub API

TODOs:

1. [DONE] Add submit button
2. [DONE] Add a checkbox for searching Github and Gitlab. Both checkboxes can be selected together, in this case 2 cards will display users profiles in each system.
3. Add different styling for github and gitlab cards
4. [DONE] Add button for author's gits
5. [DONE] Add link on user name leading to it's profile page on github or gitlab
6. Add more user data, like: created date, social profiles links, email, job title
7. Add nice animation for cards loading
8. [DONE] Style properly:
   - Space betwee the 2 cards
   - Checkboxes - enlarge
9. [DONE] Add footer
10. GitLab now requires authentication to fetch user repositories