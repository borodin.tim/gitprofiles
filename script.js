const APIURL_GITHUB = 'https://api.github.com/users/';
const APIURL_GITLAB = 'https://gitlab.com/api/v4/users';

const form = document.getElementById('form');
const searchInput = document.getElementById('search');
const main = document.getElementById('main');
const chkboxGithub = document.getElementById('github_chkbox');
const chkboxGitlab = document.getElementById('gitlab_chkbox');
const searchBtn = document.querySelector('.search-btn');
const authorGitsBtn = document.querySelector('.author-gits-btn');

const githubName = 'GitHub';
const gitlabName = 'GitLab';
const searchPlaceholderText = "Search a GitHub or GitLab User";
const allCheckboxesUncheckedMsg = 'Please select any git SCM!';

searchInput.placeholder = searchPlaceholderText;

authorGitsBtn.addEventListener('click', async () => {
    cleanAndGetUser('de1ain', 'borodin.tim');
})

chkboxGithub.addEventListener('change', () => {
    searchBtn.disabled = false;
    authorGitsBtn.disabled = false;
    if (!chkboxGithub.checked) {
        if (!chkboxGitlab.checked) {
            searchInput.placeholder = allCheckboxesUncheckedMsg;
            searchBtn.disabled = true;
            authorGitsBtn.disabled = true;
        } else {
            searchInput.placeholder = searchInput.placeholder.replace('GitHub ', '');
        }
    } else {
        if (searchInput.placeholder === allCheckboxesUncheckedMsg) {
            searchInput.placeholder = "Search a GitHub User";
        } else {
            searchInput.placeholder = searchInput.placeholder.replace('Search a', 'Search a GitHub');
        }
    }
    fixPlaceholderText();
});

chkboxGitlab.addEventListener('change', () => {
    searchBtn.disabled = false;
    authorGitsBtn.disabled = false;
    if (!chkboxGitlab.checked) {
        if (!chkboxGithub.checked) {
            searchInput.placeholder = allCheckboxesUncheckedMsg;
            searchBtn.disabled = true;
            authorGitsBtn.disabled = true;
        } else {
            searchInput.placeholder = searchInput.placeholder.replace('GitLab ', '');
        }
    } else {
        if (searchInput.placeholder === allCheckboxesUncheckedMsg) {
            searchInput.placeholder = "Search a GitLab User"
        } else {
            searchInput.placeholder = searchInput.placeholder.replace('User', 'GitLab User');
        }
    }
    fixPlaceholderText();
});

function fixPlaceholderText() {
    if (!searchInput.placeholder.includes('GitHub') || !searchInput.placeholder.includes('GitLab')) {
        searchInput.placeholder = searchInput.placeholder.replace(' or', '');
    }
    if (searchInput.placeholder.includes('GitHub') && searchInput.placeholder.includes('GitLab')) {
        searchInput.placeholder = searchPlaceholderText;
    }
}

async function getUser(usernameGithub, usernameGitlab) {
    await getUserFromGithub(usernameGithub);
    await getuserFromGitlab(usernameGitlab ? usernameGitlab : usernameGithub);
}

async function getUserFromGithub(username) {
    try {
        if (chkboxGithub.checked) {
            const { data } = await axios.get(APIURL_GITHUB + username);
            createUserCard(githubName, data.avatar_url, data.login, data.bio, data.followers, data.following, data.public_repos, data.html_url, data.name);
            getRepos(username);
        }
    } catch (err) {
        if (err.response.status === 404) {
            createErrorCard(githubName, 'No profile found with this username');
        }
    }
}

async function getuserFromGitlab(username) {
    try {
        if (chkboxGitlab.checked) {
            const { data } = await axios.get(APIURL_GITLAB + '?username=' + username);
            if (!data || data.length === 0) {
                throw new Error('Error occured');
            }
            const id = data[0].id;
            const webUrl = data[0].web_url;
            const name = data[0].name;
            // const bio = await axios.get(APIURL_GITLAB + '/' + id)
            // const projects = { data: ['a'] }//await axios.get(APIURL_GITLAB + '/' + id + '/projects');
            createUserCard(gitlabName, data[0].avatar_url, data[0].username, null, null, null, null, null, name);
            // addReposToCardGitlab(projects.data);
        }
    } catch (err) {
        createErrorCard(gitlabName, 'No profile found with this username');
        console.log(err)
    }
}

async function getRepos(username) {
    try {
        const { data } = await axios.get(APIURL_GITHUB + username + '/repos?sort=created');
        addReposToCardGithub(data);
    } catch (err) {
        createErrorCard(null, 'Problem fetching repos');
    }
}

function createUserCard(gitName, avatarUrl, login, bio = null, followers = null, following = null, publicRepos = null, webUrl = null, personName) {
    const cardHTML = `
    <div class="card">
        <div id="git-name">${gitName}</div>
        <div><img src="${avatarUrl}" class="avatar" /></div>
        <div class="user-info">
        <h2><a  class="login" href="${webUrl}" target="_blank">${login}</a></h2>
        <div id="person-name">${personName}</div>
        <p>${bio ? bio : ''}</p>
        <ul>
            ${followers !== null ? `<li>${followers} <strong>Followers</strong></li>` : ''}
            ${following !== null ? `<li>${following} <strong>Following</strong></li>` : ''}
            ${publicRepos ? `<li>` + publicRepos + `<strong>Repos</strong></li>` : ''}
        </ul>
        <div id="repos${gitName.toLowerCase()}"></div>
        </div>
    </div>`;
    main.innerHTML += cardHTML;
}

function addReposToCardGithub(repos) {
    const reposEl = document.getElementById(`repos${githubName.toLowerCase()}`);
    repos
        .slice(0, 10)
        .forEach(repo => {
            const repoEl = document.createElement('a');
            repoEl.classList.add('repo');
            repoEl.href = repo.html_url;
            repoEl.target = '_blank';
            repoEl.innerText = repo.name;
            reposEl.appendChild(repoEl);
        });
}

function addReposToCardGitlab(repos) {
    const reposEl = document.getElementById(`repos${gitlabName.toLowerCase()}`);
    repos
        .slice(0, 10)
        .forEach(repo => {
            const repoEl = document.createElement('a');
            repoEl.classList.add('repo');
            repoEl.href = repo.web_url;
            repoEl.target = '_blank';
            repoEl.innerText = repo.name;
            reposEl.appendChild(repoEl);
        });
}

function createErrorCard(gitName = null, errorMsg) {
    const cardHTML = `
    <div class="card"> 
        <div id="git-name">${gitName}</div>
        <h1>${errorMsg}</h1>
    </div>
    `;

    main.innerHTML += cardHTML;
}

form.addEventListener('submit', (e) => {
    e.preventDefault();
    const user = searchInput.value;

    if (user) {
        cleanAndGetUser(user, null);
    }
});

function cleanAndGetUser(user1, user2) {
    cleanPreviousResults();
    getUser(user1, user2);
}

function cleanPreviousResults() {
    searchInput.value = '';
    main.innerHTML = '';
}